
<div>

	<script src='/js/jquery-2.2.3.js'></script>
 <?php  include('include/training/easyXss.php'); ?>


	<h2 class="lead text-center"> Practica "XSS": v1 </h2>
		
	<form class="form-inline" role="form" id="basicXss1">

		<div class="form-group"  >  
			<input name="msg" id="msgX" type="text" class="form-control" placeholder="Introduce mensaje">
		</div>

		<select class="form-group input-sm btn-success" id="setMETHOD">
			  <option value="GET"  class="success"> GET  </option>
			  <option value="POST" class="success"> POST </option>
		</select>
	  	<input type="submit" class="btn btn-danger" value="[ dame un XSS ]" >
	</form>	

	<br>
	
	<hr>  <div id="printScript"> </div> <hr>

	<!-- ########################################################## -->


<h2 class="lead text-center"> Practica "XSS": v2 </h2>
	<form method="GET" class="form-inline" action="include/training/easyXss.php"  role="form" >

		<div class="form-group"  >
			<input name="MSG2" type="text" class="form-control" placeholder="Introduce mensaje">
		</div>
	  		<input type="submit" class="btn btn-danger" value="[ dame un XSS ]" >
	</form>
	
	<hr>  
	<?php

		if (isset($_GET['MSG']) ){  
				$mensaje = $_GET['MSG']; 
				$o  	 = testAudit($mensaje);
				print $o; 
		}
	?>	
	<hr>
		
	<!-- ########################################################## -->
</div>	

	<!-- 
		# memo:
		<meta http-equiv='Refresh' content='0;url=http://www.google.com'>
	-->