      <!--    <div class="inner cover">
	            <h1 class="cover-heading">Alf@lfas Web</h1>
				<h3 class="cover-heading">Features</h3>

	            <img src="img/ezelf.jpg" alt="" class="lead">
	            <p class="lead">Proyecto en donde se simula una web de apariencia simple. En nuestro supuesto, de alguna manera algun "fulano", a conseguido vulnerar el portal y dejarar algo extra de JS.</p>
	            <p class="lead">
	              <a href="#" class="btn btn-lg btn-default">Learn more</a>
	            </p>
		          <div id="setIframe" class="lead">
		          </div>
          </div>
<div>
	<b >Function JS:"discoverHost()"</b>
	<h3>Descubriendo host vivos desde el cliente:</h3>
</div>
-->

    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner carousel-heigth" role="listbox">

        <div class="item active">
          <img class="first-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="First slide">
          <div class="container-fluid">
            <div class="carousel-caption" style="color:#fff;">
              <h1>Whois is my client?</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Saber mas...</a></p>
            </div>
          </div>
        </div>

        <div class="item">
          <img class="second-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Second slide">
          <div class="container-fluid">
            <div class="carousel-caption">
              <h1>Function JS:"discoverHost()"</h1>
              <p>Descubriendo host vivos desde el cliente. Evadiendo/bypasseando el "Cross Domain"</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Saber mas...</a></p>
            </div>
          </div>
        </div>

        <div class="item">
          <img class="third-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide">
          <div class="container-fluid">
            <div class="carousel-caption">
              <h1>Quiero tu pulso</h1>
              <p>Simple Keylogger con JS</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Saber mas...</a></p>
            </div>
          </div>
        </div>
      </div>
     
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
      </a>

    </div><!-- /.carousel -->
