
<div id="container-Proyect">
	
<div class="container-fluid" id="badJS-Proyect">
	<h1>B@d JS: <small>"Estamos de tu lado"</small> </h1>
	<h3>Detalles del proyecto</h3>
<div class="disclaimer-BadJS">
	<hr>
	<h3>DISCLAIMER</h3>
	<p class="lead ">Todo con fines puramente academicos. </p>
	<p class="text-justify"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam laboriosam aspernatur incidunt ipsum reiciendis culpa accusantium ipsa ad et tenetur, unde tempore magni, mollitia nulla doloremque. Architecto facere illo dicta.</p>
	<hr>
</div>

<div class="introBadJS">
	<h4 class="text-left">INTRODUCCION:</h4>
	<p class="text-left">===================</p>
	<p class="lead">
		El foco de este proyecto al que llamo: "b@dJS", esta en explotar las posibilidades ofrecidas por Javascript y el partido que un atacante podra sacarle.
	</p>
	<p>
		Experimentaremos con las distintas alternativas que puede ofrecerle Javascript a un atacante.
		Tomando como base la explotacion de un XSS (cualquiera de sus variantes ) que permita embeber javascript y ser "comido" por el browser. 
	</p>
	<p>
		Sabemos que el visitar un sitio web a travez de nuestro navegador, implica enviar una peticion al servidor que hostea el sitio, esa accion sera respondida con el codigo conformado por HTML y JavaScript, codigo que tomara e interpretara nuestro navegador.
	</p>
	<p>
		Si nos enfocamos en javascript, estamos hablando de un lenguaje de codigo que sera interpretado(no compilado!) por el navegador(client-side). Si bien existe una version de Javascript del lado del servidor (Server-side: Node.js), escapa al objeto de proyecto.
	</p>
</div>	

<div class="ambitoJS">
	<h4 class="text-left">AMBITO DE JAVASCRIPT</h4>
	<p class="text-left">===========================</p>
	<p>
		JavaScript fue diseñado de forma que se ejecutara en un entorno muy limitado que permitiera a los usuarios confiar en la ejecución de los scripts. De esta forma sus scripts no pueden comunicarse con recursos que no pertenezcan al mismo dominio desde el que se descargó el script. 
	</p>
	<p>
		Los scripts tampoco pueden cerrar ventanas que no hayan abierto esos mismos scripts. Las ventanas que se crean no pueden ser demasiado pequeñas ni demasiado grandes ni colocarse fuera de la vista del usuario (aunque los detalles concretos dependen de cada navegador).
		Además, los scripts no pueden acceder a los archivos del ordenador del usuario (ni en modo lectura ni en modo escritura) y tampoco pueden leer o modificar las preferencias del navegador.
	</p>
	<p>
		Por último, si la ejecución de un script dura demasiado tiempo (por ejemplo por un error de programación -talvez un bucle infinito-) el navegador informa 
		al usuario de que un script está consumiendo demasiados recursos y le da la posibilidad de detener su ejecución.
	</p>
	<p>
		A pesar de todo, existen alternativas para poder saltarse algunas de las limitaciones anteriores. (La alternativa más utilizada y conocida consiste en firmar digitalmente el script y solicitar al usuario el permiso para realizar esas acciones.)
	</p>
	
</div>

<div class="same-origin">
	<h4 class="text-left">SAME-ORIGIN POLICY</h4>
	<p class="text-left">=====================</p>
	
	<p>
		Javascript trae consigo una limitacion o mas bien una imposicion por parte de los navegadores, que yo mismo desconocia hasta hace no mucho por lo que considero importante mencionar el tema: 
	</p>
	<p>	
		Al intercambio de peticiones entre distintos dominios se denomina "CROSSDOMAIN". Los navegadores actuales llevan un mencanismo de seguridad (Same-Origin Policy )que impide que desde javascript se puedan hacer este tipo de peticiones.
	</p>
	<p>
		Esta política hace que el código que se carga en una pestaña de un navegador pueda acceder solo a datos de páginas del mismo dominio, es decir, que un código JavaScript no podría nunca acceder a la cookie o ningún elemento que compongan la página servida desde otro dominio. Es por ello que para poder acceder a los datos de otra pestaña se utilizan, por ejemplo, los ataques de Cross-Site Scripting, ya que consiguen inyectar código en la pestaña del dominio víctima.

	</p>
</div>

<div class="isXss">
	
	<h4 class="text-left">QUE ES UN "XSS"?</h4>
	<p class="text-left">=====================</p>
	
	<p>
		XSS o Cross-Site Scripting es una vulnerabilidad ya conocida desde hace ya mucho tiempo. Junto a las famosas SQLi(inyecciones de SQL)
		lideran el top de los fallos mas habituales, por lo que se les otorgo un lugar privilegiado dentro del TOP 10 de OWASP.
	</p>
	<p>
		Son ataques que sacan ventajan de la poco (o nula) validacion de de datos que son ingresados al sitio. Pensemos en posibles entradas de datos; foros, chats, libros de visita, foros, el espacio que muestra los usuarios conectados,etc..., las posibilidades son muchas (proporcional a la imaginacion del atacante)
	</p>
	<p>
		Esta técnica de ataque forzara a un sitio web a ejecutar el código suministrado por un atacante.
		Un uso interesate para la explotacion de XSS seria el robo de cookies de session, en donde nos valdremos de la propiedad <code>document.cookie</code>.  
	</p>

	<h5 class="lead">TIPOS DE XSS:</h5>
	<p class="text-left">PERSISTENTE:</p>
	<p>
		Un Cross-Site Script persistente involucra a aquellas inyecciones que quedar almacenadas en el portal web, afectando a todas las personas que visiten la pagina. 
	</p>

	<p class="text-left">REFLEJADO</p>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat cupiditate accusamus inventore rem. Eius necessitatibus illo illum laboriosam vero maiores, quis, ipsum itaque quibusdam tempora recusandae! Reiciendis, facilis! Nulla, architecto.
	</p>
</div>
<div class="targetcomun">
	
	<h5 class="lead">UN OBJETIVO EN COMUN</h5>
	<p>
		Como tecnica de ataque para llegar al usuario/cliente antes hice referencia a los XSS, por el ser muy probablemente el fallo con el cual el ciberdelincuente se enfrentente mas a menudo.
	</p>
	<p>		
		Pero sin perder foco remarco que los ciberatacantates buscaran ejecutar codigo en el navegador de usuario. Al representar al atacante la tecnica empleada para hacer llegar ese codigo al usuario no es tan relevante, como el objetivo en si (incluir codigo...). 
	</p>
	<p>
		Quiero decir decir; si un atacante descubre un XSS, bienvenido sea, si se tiene acceso a directorio raiz del portal atravez de alguna credencial FTP o bypaseamos el formulario de login del panel de administracion de el sitio web o lo que se les ocurra para incluir codigo ajeno a los propositos originales del portal, como atacantes nuestra meta esta cumplida. 

	</p>
</div>

		<h5 class="lead">Y AHORA QUE ?</h5>
	<p class="text-justify">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae officiis sunt quos asperiores maxime nemo architecto, ducimus, voluptatum nisi numquam. Odio officiis sequi ad voluptatibus magni, voluptate dignissimos id vel.
	</p>
</div>

</div>
