  <!DOCTYPE html>
  <html lang="es" >
  <head>
<!-- ##// ############################## HEADER ################################################################## //##-->
	<?php include("include/header.php"); ?>
<!-- ##// ############################## ###### ################################################################## //##-->
</head>
<body class="home" >
  <div class="site-wrapper">
      <div class="site-wrapper-inner" >
        <div class="cover-container" >


        <!-- <div id="particles-js"></div> -->

<!-- // ############################## NAVBAR ################################################################## //## -->
      <?php include("include/navbar.php"); ?>
<!-- // ######################################################################################################## //## -->
<?php
echo "";
  if(isset($set_page) && $set_page =='home') {
        include('include/body/home.php');
      } 

  elseif(isset($set_page) && $set_page =='project') {
      include('include/body/project.php');
    } 

  elseif(isset($set_page) && $set_page =='features') {
      include('include/body/features.php');
    } 
            # ## inicio Feactures #############################
            elseif(isset($set_page) && $set_page =='getclient'    ) { include('include/features/getclient.php'    ); } 

            elseif(isset($set_page) && $set_page =='scanlocalhost') { include('include/features/scanlocalhost.php'); } 

            elseif(isset($set_page) && $set_page =='keylogger'    ) { include('include/features/keylogger.php'    ); } 

            elseif(isset($set_page) && $set_page =='testing'    ) { include('include/features/testingJS.php'    ); } 


            # ## fin de Feactures #############################

  elseif(isset($set_page) && $set_page =='training') {
      include('include/body/training/details.php');
    } 
    # ## inicio trainings #############################
      elseif(isset($set_page) && $set_page =='UNO'    ) { include('include/training/uno.php'    ); } 
      elseif(isset($set_page) && $set_page =='DOS'    ) { include('include/training/dos.php'    ); } 
      elseif(isset($set_page) && $set_page =='TRES'    ) { include('include/training/tres.php'    ); } 
      elseif(isset($set_page) && $set_page =='CUATRO'    ) { include('include/training/cuatro.php'    ); } 


     # ## inicio trainings #############################

  elseif(isset($set_page) && $set_page =='contact') {
      include('include/body/Contact.php');
    } 

  else { include('include/body/home.php'); }
?>
            

<!-- // ############################## FOOTER ################################################################## //##-->
      <?php 

      if(isset($set_page) && $set_page !='contact') {
            include("include/footer.php"); 
        } 

      ?>

<!-- // ######################################################################################################## //##-->
        </div>
      </div>
    </div>

<!-- ##// ############################## SCRIPTS ################################################################## //##-->
      <?php  include("include/footer-scripts.php"); ?>
<!-- ##// ############################## ####### ################################################################## //##-->

</body>
</html>

