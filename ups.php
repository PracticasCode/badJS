<?php

function generateRandomString($length = 10,$tag = 1) {
	$title = '!@#$%^&*()_+=/' ;
	$alpha = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$nums = '123456';

	#$x = [];
	$x[0] = $title;
	$x[1] = $alpha;
	$x[3] = $nums;

	#print $x[$tag];
    return substr(str_shuffle(str_repeat($x[$tag], ceil($length/strlen($x[$tag])) )),1,$length);
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv='Refresh' content='5;url=/index.php'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-theme.min.css" >
	<link rel="stylesheet" href="../css/ie10-viewport-bug-workaround.css" >
	<link href="favicon.ico" rel="shortcut icon">
	<link rel="stylesheet" href="../css/cover.css">	
	<link rel="stylesheet" href="../css/styleHome.css">
	<title> <?php generateRandomString(generateRandomString(2,3), 0); ?> </title>
</head>
<body id="error">

	<a href="/index.php"><img src="../../img/gif/400.gif" alt="no, no, nooo..!!" id="chao"></a> 
<?php

	print "
<div style='display:none;'>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

".generateRandomString(generateRandomString(4,3), 1)."
<div>
";


?>
</body>
</html>
